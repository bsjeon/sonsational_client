package com.jihye115.jbspsw.sonsational;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RequestedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RequestedFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class RequestedFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_requested, container, false);

        return v;
    }
}
